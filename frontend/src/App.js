import {
  BrowserRouter,
  Routes,
  Route,
  Link
} from "react-router-dom";
import './App.css';
import Food from './pages/food/Food.js'
import Order from './pages/order/Order.js'
import Client from './pages/client/Client.js'
import ChangeOrder from './pages/order/ChangeOrder.js'
import { useParams } from 'react-router-dom'


function App() {

  return (
    <BrowserRouter>
        <div>

          <Routes>
            <Route path="/" element={<Home />}>
            </Route>
            <Route path="/food" element={<Food />}>
            </Route>
            <Route path="/orders" element={<Order/>}>
            </Route>
            <Route path="/clients" element={<Client />}>
            </Route>
            <Route path="/changeOrder/:id" element={<ChangeOrder/>}>
            </Route>
          </Routes>
        </div>
      </BrowserRouter>
  );
}


function Home() {
  return <div className="FlexContainer">
    <div className="NavigateButton CircleButton"><a href='/food'><span>Продукты</span></a></div>
    <div className="NavigateButton CircleButton"><a href='/orders'><span>Заказы</span></a></div>
    <div className="NavigateButton CircleButton"><a href='/clients'><span>Клиенты</span></a></div>
  </div>;
}


export default App;
