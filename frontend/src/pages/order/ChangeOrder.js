import { useState, useEffect } from 'react'
import { useParams } from 'react-router-dom'
import {Form, Button} from 'react-bootstrap'
import Table from 'react-bootstrap/Table'


function getOrder(setData, orderId, setState) {

    fetch('http://127.0.0.1:8000/order/'+orderId)
        .then(res => res.json())
        .then(data => {
            console.log(data.id)
            setData([data])
            setState(data.state)
        })

}

function GetFoodInOrder(id, setFoodInOrder, setTotalPrice, setSuccess) {
    if (!id) {
        return null
    }
    fetch(`http://127.0.0.1:8000/foodInOrder?order_id=${id}`, {
        method: 'GET',
    }).then(e => e.json())
      .then(e => {
          console.log(e.detail == undefined)
          if (e.detail == undefined) {
          setFoodInOrder(e)
          setSuccess(true)
          for (var i = 0, a = 0; i < e.length; i++) {a+=e[i].price*e[i].count}
          console.log(a.toFixed(2))
          setTotalPrice(a.toFixed(2))}
        })
      .catch(e => setSuccess(false))
}

function getStates(setStates) {
    fetch('http://127.0.0.1:8000/states/')
        .then(res => res.json())
        .then(data => {
            setStates(data)
            console.log(data)
        })
}

function changeOrderStatus(orderId, status) {
    fetch(`http://127.0.0.1:8000/changeOrderStatus?order_id=${orderId}&changed_status=${status}`, {
        method: 'POST',
    }).catch(e => console.log(e))
}

function AddFood(foodId, count, orderId, setFoodInOrder, setTotalPrice, setSuccess) {
        fetch(`http://127.0.0.1:8000/addToOrder?order_id=${orderId}&food_id=${foodId}&count=${count}`, {
        method: 'POST',
    }).then(() => GetFoodInOrder(orderId, setFoodInOrder, setTotalPrice, setSuccess))
}

function GetAllFood(setAllFood) {
    fetch(`http://127.0.0.1:8000/all_food/`, {
        method: 'GET',
    }).then(e => e.json())
      .then(e => {
          console.log(e)
          setAllFood(e)
        })
      .catch(e => console.log(e))
}

function ChangeOrder() {
  const [data, setData] = useState([])
  const [allFood, setAllFood] = useState([])
  const [state, setState] = useState(false)
  const [states, setStates] = useState([])
  const [foodChanged, setFoodChanged] = useState(false)
  const [foodId, setFoodId] = useState(0)
  const [count, setCount] = useState(0)
  const [foodInOrder, setFoodInOrder] = useState([])
  const [totalPrice, setTotalPrice] = useState(0)
  const [success, setSuccess] = useState(false)

  const all_food = allFood.map(x => (<option value={x.id} key={x.id}>{x.id} - {x.name}</option>))
  const all_food_in_order = foodInOrder.map(x => (<tr><td>{x.food_id}</td><td>{x.count}</td><td>{x.price}</td></tr>))
  const order = data.map(x => (<div><p>{x.date}</p><p>{x.state}</p><p>{x.client.name}</p><p>{x.client.phone}</p></div>))

  const params = useParams();
  const orderId = params.id;

  useEffect(() => {
    GetAllFood(setAllFood)
    getOrder(setData, orderId, setState)
    GetFoodInOrder(orderId, setFoodInOrder, setTotalPrice, setSuccess)
    getStates(setStates)
  }, false)

  const handleSubmit = () => {
            changeOrderStatus(orderId, states[states.indexOf(state)+1])
          }

  if (state == states[0]) {
      return (
      <div>
      <div><p>{order}</p></div>
      {success && <details>
      <summary>Еда в заказе</summary>
            <Table striped bordered hover>
            <thead>
                          <tr>
                              <th>Id еды</th>
                              <th>Количество еды</th>
                              <th>Цена</th>
                          </tr>
                      </thead>
                      <tbody>
                      {all_food_in_order}
                      </tbody>
            </Table>
            <p>Итоговая цена - {totalPrice}</p>
      </details>}
      <form onSubmit={handleSubmit}>
                    <Form.Group className="mb-3" controlId="formBasicFood">
                        <Form.Label>Id еды:</Form.Label>
                        <Form.Select aria-label="Default select example" onChange={e => {setFoodChanged(true);setFoodId(e.target.value)}}>
                            {!foodChanged && <option value={null} key={0}>Выберите еду:</option>}
                            {all_food}
                        </Form.Select>
                    </Form.Group>
                    <Form.Group className="mb-3" controlId="formBasicCount">
                        <Form.Label>Количество еды:</Form.Label>
                        <Form.Control type="number" placeholder="Введите количество выбранной еды" value={count} onChange={e => setCount(e.target.value)}/>
                    </Form.Group>
                    <AddFoodButton setFoodInOrder={setFoodInOrder} setTotalPrice={setTotalPrice} setSuccess={setSuccess} foodId={foodId} count={count} orderId={orderId} setSuccess={setSuccess} setTotalPrice={setTotalPrice} setFoodInOrder={setFoodInOrder}/>
                    <SendButton text={"Начать приготовление заказа"}/>
      </form>
      <p className="NavigateButton"><a href='/orders'>Назад</a></p>
      </div>
  )} else {
      return (
      <div>
      <div><p>{order}</p></div>
      {success && <details>
      <summary>Еда в заказе</summary>
                <Table striped bordered hover>
                <thead>
                              <tr>
                                  <th>Id еды</th>
                                  <th>Количество еды</th>
                                  <th>Цена</th>
                              </tr>
                          </thead>
                          <tbody>
                          {all_food_in_order}
                          </tbody>
                </Table>
                <p>Итоговая цена - {totalPrice}</p>
      </details>
        }
      {state != states[states.length-1] && <form onSubmit={handleSubmit}><SendButton handeSubmit={() => {handleSubmit()}} text={`Сменить состояние заказа на ${states[states.indexOf(state)+1]}`}/></form>}
      {state == states[states.length-1] && <div style={{border: '2px solid black', width: '10vmax', textAlign: 'center'}}>Заказ завершён</div>}
      <p className="NavigateButton"><a href='/orders'>Назад</a></p>
      </div>
      )
  }
}

function AddFoodButton(props) {
            return (
                <Button variant="primary" onClick={() => {AddFood(props.foodId, props.count, props.orderId, props.setFoodInOrder, props.setTotalPrice, props.setSuccess)}}>Добавить в заказ</Button>
            )
        }

function SendButton(props) {
            return (
                <Button variant="primary" type="submit"  onClick={props.handleSubmit}>{props.text}</Button>
            )
        }

export default ChangeOrder;
