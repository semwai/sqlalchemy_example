import Table from 'react-bootstrap/Table'
import { useState, useEffect } from 'react'
import { Container, Row, Col} from 'react-bootstrap'
import {
  BrowserRouter,
  Routes,
  Route,
  Link
} from "react-router-dom";

import AddOrder from './AddOrder'
import OrderById from './OrderById'
import OrdersByFilter from './OrdersByFilter'


function getOrders(setData) {

    fetch('http://127.0.0.1:8000/orders/')
        .then(res => res.json())
        .then(data => {
            setData(data)
        })

}


function Order() {
  const [data, setData] = useState([])

  const orders = data.map(x => (<a href={"changeOrder/"+x.id}><div><p>{x.id} - {x.client.name}</p></div></a>))

  useEffect(() => {
    getOrders(setData)
  }, false)

  return <div>
    <Container>
    <details>
    <summary>Orders</summary>
      <Row>
        <Col className="FlexContainer">
          {orders}
        </Col>
      </Row>
    </details>
      <Row>
      <Col>
        <hr/>
        <OrderById data={data}/>
        <hr/>
        <AddOrder method={() => getOrders(setData)}/>
        <hr/>
        <OrdersByFilter method={() => getOrders(setData)}/>
        </Col>
      </Row>
    </Container>
    <p className="NavigateButton"><a href='/'>На главную</a></p>
  </div>;
}


export default Order;
