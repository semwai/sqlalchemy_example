import {Form, Button} from 'react-bootstrap'
import Table from 'react-bootstrap/Table'
import { useState, useEffect } from 'react'


function GetFilteredOrders(update, setFilteredOrders, filter) {
    fetch(`http://127.0.0.1:8000/orders?filter=${filter}`, {
        method: 'GET',
    }).then(e => e.json())
      .then(e => {
          console.log(e)
          setFilteredOrders(e)
          update()
        })
      .catch(e => console.log(e))
}

function GetStatuses(setData, setStatus) {
    fetch('http://127.0.0.1:8000/states')
        .then(res => res.json())
        .then(data => {
            console.log(data)
            setData(data)
            setStatus(data[0])
        })
}

function OrderByFilter(p) {
    const [filter, setFilter] = useState('')
    const [filteredOrders, setFilteredOrders] = useState([])
    const [orders, setOrders] = useState([])
    const [data, setData] = useState([])
    const [status, setStatus] = useState([])
    const [success, setSuccess] = useState(false)

    const statuses = data.map(x => (<option value={x} key={x}>{x}</option>))
    const ordersByFilter = filteredOrders.map(x => (<tr><td>{x.date}</td><td>{x.state}</td><td>{x.client.name}</td><td>{x.client.phone}</td></tr>))

    const submit = (e) => {
        e.preventDefault()
        GetFilteredOrders(p.method, setFilteredOrders, filter)
        setSuccess(true)
    }

    useEffect(() => {
        GetStatuses(setData, setStatus)
    }, [])

    return <Form onSubmit={submit}>
    <Form.Group className="mb-3" controlId="formBasicStatus">
      <Form.Label>Статус заказа</Form.Label>
      <Form.Select aria-label="Default select example" onChange={e => setFilter(e.target.value)}>
        {statuses}
      </Form.Select>
    </Form.Group>
    <Button variant="primary" type="submit">
      Подтвердить
    </Button>
    {success &&
    <Table striped bordered hover>
    <thead>
                  <tr>
                      <th>Дата</th>
                      <th>Состояние заказа</th>
                      <th>Имя клиента</th>
                      <th>Телефон клиента</th>
                  </tr>
              </thead>
              <tbody>
              {ordersByFilter}
              </tbody>
    </Table>}
  </Form>

}

export default OrderByFilter;