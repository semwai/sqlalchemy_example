import {Form, Button} from 'react-bootstrap'
import { useState, useEffect } from 'react'
import { Navigate } from 'react-router-dom';

var order_id

function PostOrder(userId, setRedirect) {
        fetch(`http://127.0.0.1:8000/order?user_id=${userId}`, {
        method: 'POST',
    }).then(e => e.json())
    .then(e => order_id = e.detail)
      .then(e => setRedirect(true))
      .catch(e => console.log(e))

}

function GetAllClients(update, setAllClients) {
    fetch(`http://127.0.0.1:8000/clients/`, {
        method: 'GET',
    }).then(e => e.json())
      .then(e => {
          console.log(e)
          setAllClients(e)
          update()
        })
      .catch(e => console.log(e))
}

function GetAllFood(update, setAllFood) {
    fetch(`http://127.0.0.1:8000/all_food/`, {
        method: 'GET',
    }).then(e => e.json())
      .then(e => {
          console.log(e)
          setAllFood(e)
          update()
        })
      .catch(e => console.log(e))
}

    function AddOrder(p) {
        const [currentStep, setCurrentStep] = useState(1)
        const [userId, setUserId] = useState(0)
        const [allClients, setAllClients] = useState([])
        const [foodId, setFoodId] = useState(0)
        const [additionalFoodId, setAdditionalFoodId] = useState(0)
        const [allFood, setAllFood] = useState([])
        const [count, setCount] = useState(0)
        const [additionalCount, setAdditionalCount] = useState(0)
        const [orderCreated, setOrderCreated] = useState(false)
        const [foodChanged, setFoodChanged] = useState(false)
        const [clientChanged, setClientChanged] = useState(false)
        const [redirect, setRedirect] = useState(false)

        const all_clients = allClients.map(x => (<option value={x.id} key={x.id}>{x.id} - {x.name}</option>))
        const all_food = allFood.map(x => (<option value={x.id} key={x.id}>{x.id} - {x.name}</option>))

        const handleSubmit = (e) => {
            e.preventDefault()
            PostOrder(userId, setRedirect)
            setOrderCreated(true)
          }

        useEffect(() => {
            GetAllClients(p.method, setAllClients)
            GetAllFood(p.method, setAllFood)
        }, [])

        return (
          <div>
          <p>Шаг {currentStep} </p>
          <form onSubmit={handleSubmit}>
            <CreateOrderStep
              setUserId={setUserId}
              all_clients={all_clients}
              clientChanged={clientChanged}
              setClientChanged={setClientChanged}
              redirect={redirect}
            />
            <p>
            <SendButton/>
            </p>

          </form>
          </div>
        );
      }

    function SendButton(props) {
            return (
                <Button variant="primary" type="submit">Подтвердить</Button>
            )
        }

    function CreateOrderStep(props) {
          return(
            <div>
              <Form.Group className="mb-3" controlId="formBasicClient">
                <Form.Label>Id клиента:</Form.Label>
                <Form.Select aria-label="Default select example" onChange={e => {props.setClientChanged(true);props.setUserId(e.target.value)}}>
                    {!props.clientChanged && <option value={null} key={0}>Выберите клиента:</option>}
                    <option value={0}></option>
                    {props.all_clients}
                </Form.Select>
              </Form.Group>
              { props.redirect ? (<Navigate push to={"/changeOrder/"+order_id}/>) : null }
            </div>
          );
    }


export default AddOrder