import {Form, Button} from 'react-bootstrap'
import Table from 'react-bootstrap/Table'
import { useState, useEffect } from 'react'


function GetOrder(id, setOrder) {
    if (!id) {
        return null
    }
    fetch(`http://127.0.0.1:8000/order/${id}`, {
        method: 'GET',
    }).then(e => e.json())
      .then(e => {
          console.log(e)
          setOrder(e)
        })
      .catch(e => console.log(e))
}

function GetFoodInOrder(id, setFoodInOrder, setTotalPrice, setSuccess) {
    if (!id) {
        return null
    }
    fetch(`http://127.0.0.1:8000/foodInOrder?order_id=${id}`, {
        method: 'GET',
    }).then(e => e.json())
      .then(e => {
          console.log(e)
          setFoodInOrder(e)
          for (var i = 0, a = 0; i < e.length; i++) {a+=e[i].price}
          console.log(a)
          setTotalPrice(a)
          setSuccess(true)
        })
      .catch(e => setSuccess(false))
}

function GetAllOrders(setAllOrders) {
    fetch(`http://127.0.0.1:8000/orders/`, {
        method: 'GET',
    }).then(e => e.json())
      .then(e => {
          console.log(e)
          setAllOrders(e)
        })
      .catch(e => console.log(e))
}

function OrderById(p) {
    const [id, setId] = useState(0)
    const [order, setOrder] = useState(false)
    const [foodInOrder, setFoodInOrder] = useState([])
    const [totalPrice, setTotalPrice] = useState(0)
    const [allOrders, setAllOrders] = useState([])
    const [success, setSuccess] = useState(false)
    const [changed, setChanged] = useState(false)

    const all_orders = p.data.map(x => (<option value={x.id} key={x.id}>{x.id} - {x.client.name}</option>))
    const all_food_in_order = foodInOrder.map(x => (<tr><td>{x.food_id}</td><td>{x.count}</td><td>{x.price}</td></tr>))

    const submit = (e) => {
        e.preventDefault()
        GetOrder(id, setOrder)
        GetFoodInOrder(id, setFoodInOrder, setTotalPrice, setSuccess)
    }

    useEffect(() => {
        GetAllOrders(setAllOrders)
  }, [])

    return <Form onSubmit={submit}>
    <Form.Group className="mb-3" controlId="formBasicStatus">
      <Form.Label>Заказ по id</Form.Label>
      <Form.Select aria-label="Default select example" onChange={e => {setChanged(true);setId(e.target.value)}}>
        {!changed && <option value={null} key={0}>Выберите заказ:</option>}
        {all_orders}
      </Form.Select>
    </Form.Group>
    <Button variant="primary" type="submit">
      Подтвердить
    </Button>
    {order &&
    <div>
        <div>
            <h2>Заказ</h2>
            <Table striped bordered hover>
            <thead>
                          <tr>
                              <th>Дата</th>
                              <th>Состояние заказа</th>
                              <th>Имя клиента</th>
                              <th>Телефон клиента</th>
                          </tr>
                      </thead>
                      <tbody>
                      <tr><td>{order.date}</td><td>{order.state}</td><td>{order.client.name}</td><td>{order.client.phone}</td></tr>
                      </tbody>
            </Table>
        </div>
        {success && <div>
            <h2>Еда в заказе</h2>
            <Table striped bordered hover>
            <thead>
                          <tr>
                              <th>Id еды</th>
                              <th>Количество еды</th>
                              <th>Цена</th>
                          </tr>
                      </thead>
                      <tbody>
                      {all_food_in_order}
                      </tbody>
            </Table>
            <p>Итоговая цена - {totalPrice}</p>
        </div>
        }
    </div>}
  </Form>

}

export default OrderById;