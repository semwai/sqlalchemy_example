import {Form, Button} from 'react-bootstrap'
import { useState, useEffect } from 'react'


function PostFood(name, weight, price, update) {
    console.log(update)
    fetch(`http://127.0.0.1:8000/food?name=${name}&price=${price}&weight=${weight}`, {
        method: 'POST',
        body: JSON.stringify({
            name: name,
            price: price,
            weight: weight
        })
    }).then(e => e.json())
      .then(e => {
          console.log(e)
          
          update()
        })
      .catch(e => console.log(e))
}

function AddFood(p) {
    const [name, setName] = useState("")
    const [weight, setWeight] = useState(0)
    const [price, setPrice] = useState(0)

    const setRightWeight = (w) => {
        if (/^[0-9]*$/.test(w)) {
            setWeight(w)
        }
    }

    const setRightPrice = (w) => {
        if (/^[0-9]*[.,]?[0-9]*$/.test(w)) {
            setPrice(w)
        }
    }

    const submit = (e) => {
        e.preventDefault()
        PostFood(name, weight, price, p.method)
    }
    
    return <Form onSubmit={submit}>
    <Form.Group className="mb-3" controlId="formBasicName">
      <Form.Label>Название блюда</Form.Label>
      <Form.Control type="text" value={name} onChange={e => setName(e.target.value)}/>
    </Form.Group>
  
    <Form.Group className="mb-3" controlId="formBasicWeight">
      <Form.Label>Вес, гр</Form.Label>
      <Form.Control type="number" value={weight} onChange={e => setRightWeight(e.target.value)}/>
    </Form.Group>

    <Form.Group className="mb-3" controlId="formBasicPrice">
      <Form.Label>Цена, руб</Form.Label>
      <Form.Control type="number" value={price} onChange={e => setRightPrice(e.target.value)}/>
    </Form.Group>


    <Button variant="primary" type="submit">
      Подтвердить
    </Button>
  </Form>
}

export default AddFood