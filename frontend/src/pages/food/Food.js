import Table from 'react-bootstrap/Table'
import { useState, useEffect } from 'react'
import { Container, Row, Col} from 'react-bootstrap'
import {
  BrowserRouter,
  Routes,
  Route,
  Link
} from "react-router-dom";

import AddFood from './AddFood'
import FoodById from './FoodById'


function getFood(setData) {

    fetch('http://127.0.0.1:8000/all_food/')
        .then(res => res.json())
        .then(data => {
            console.log(data)
            setData(data)

        })

}

function Food() {
  const [data, setData] = useState([])

  const products = data.map(x => (<tr><td>{x.name}</td><td>{x.weight}</td><td>{x.price}</td></tr>))

  useEffect(() => {
    getFood(setData)
  }, false)

  return <div>
    <Container>
    <details>
      <summary>Food</summary>
      <Row>
        <Col>
          <Table striped bordered hover>
              <thead>
                  <tr>
                      <th>Название</th>
                      <th>Вес, гр.</th>
                      <th>Цена, руб.</th>
                  </tr>
              </thead>
              <tbody>
              {products}
              </tbody>
          </Table>
          
        </Col>
      </Row>
    </details>
      <Row>
      <Col>
        <hr/>
        <FoodById data={data}/>
        <hr/>
        <AddFood method={() => getFood(setData)}/>
        </Col>
      </Row>
    </Container>
    <p className="NavigateButton"><a href='/'>На главную</a></p>
  </div>;
}


export default Food;
