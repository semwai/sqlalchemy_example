import {Form, Button} from 'react-bootstrap'
import Table from 'react-bootstrap/Table'
import { useState, useEffect } from 'react'


function GetFood(id, update, setFood) {
    if (!id) {
        return null
    }
    fetch(`http://127.0.0.1:8000/food/${id}`, {
        method: 'GET',
    }).then(e => e.json())
      .then(e => {
          console.log(e)
          setFood(e)
          update()
        })
      .catch(e => console.log(e))
}

function GetAllFood(update, setAllFood) {
    fetch(`http://127.0.0.1:8000/all_food/`, {
        method: 'GET',
    }).then(e => e.json())
      .then(e => {
          console.log(e)
          setAllFood(e)
          update()
        })
      .catch(e => console.log(e))
}

function FoodById(p) {
    const [id, setId] = useState(0)
    const [food, setFood] = useState(false)
    const [allFood, setAllFood] = useState([])
    const [changed, setChanged] = useState(false)

    const all_food = p.data.map(x => (<option value={x.id} key={x.id}>{x.id} - {x.name}</option>))

    const submit = (e) => {
        e.preventDefault()
        GetFood(id, p.method, setFood)
    }

    return <Form onSubmit={submit}>
    <Form.Group className="mb-3" controlId="formBasicStatus">
      <Form.Label>Еда по id</Form.Label>
      <Form.Select aria-label="Default select example" onChange={(e) => {setChanged(true); setId(e.target.value)}}>
        {!changed && <option value={null} key={0}>Выберите блюдо:</option>}
        {all_food}
      </Form.Select>
    </Form.Group>
    <Button variant="primary" type="submit">
      Подтвердить
    </Button>
    {food &&
    <Table striped bordered hover>
    <thead>
                  <tr>
                      <th>Название</th>
                      <th>Вес, гр.</th>
                      <th>Цена, руб.</th>
                  </tr>
              </thead>
              <tbody>
              <tr><td>{food.name}</td><td>{food.price}</td><td>{food.weight}</td></tr>
              </tbody>

    </Table>}
  </Form>

}

export default FoodById;