import Table from 'react-bootstrap/Table'
import { useState, useEffect } from 'react'
import { Container, Row, Col} from 'react-bootstrap'
import {
  BrowserRouter,
  Routes,
  Route,
  Link
} from "react-router-dom";

import AddClient from './AddClient'
import ClientById from './ClientById'


function getClients(setData) {

    fetch('http://127.0.0.1:8000/clients')
        .then(res => res.json())
        .then(data => {
            console.log(data)
            setData(data)

        })

}

function Client() {
  const [data, setData] = useState([])

  const clients = data.map(x => (<tr><td>{x.name}</td><td>{x.phone}</td></tr>))

  useEffect(() => {

    getClients(setData)


  }, false)

  return <div>
    <Container>
    <details>
      <summary>Clients</summary>
      <Row>
        <Col>
          <Table striped bordered hover>
              <thead>
                  <tr>
                      <th>Имя</th>
                      <th>Телефон</th>
                  </tr>
              </thead>
              <tbody>
              {clients}
              </tbody>
          </Table>
          
        </Col>
      </Row>
    </details>
      <Row>
      <Col>
        <hr/>
        <ClientById data={data}/>
        <hr/>
        <AddClient method={() => getClients(setData)}/>
        </Col>
      </Row>
    </Container>
    <p className="NavigateButton"><a href='/'>На главную</a></p>
  </div>;
}


export default Client;
