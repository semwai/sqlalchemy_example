import {Form, Button} from 'react-bootstrap'
import Table from 'react-bootstrap/Table'
import { useState, useEffect } from 'react'

function GetClient(id, setClient) {
    if (!id) {
        return null
    }
    fetch(`http://127.0.0.1:8000/client/${id}`, {
        method: 'GET',
    }).then(e => e.json())
      .then(e => {
          console.log(e)
          setClient(e)
        })
      .catch(e => console.log(e))
}


function ClientById(p) {
    const [id, setId] = useState(0)
    const [client, setClient] = useState(false)
    const [allClients, setAllClients] = useState([])
    const [changed, setChanged] = useState(false)

    const all_clients = p.data.map(x => (<option value={x.id} key={x.id}>{x.id} - {x.name}</option>))

    const submit = (e) => {
        e.preventDefault()
        GetClient(id, setClient)
    }

    return <Form onSubmit={submit}>
    <Form.Group className="mb-3" controlId="formBasicStatus">
      <Form.Label>Клиент по id</Form.Label>
      <Form.Select aria-label="Default select example" onChange={e => {setChanged(true);setId(e.target.value)}}>
        {!changed && <option value={null} key={0}>Выберите клиента:</option>}
        {all_clients}
      </Form.Select>
    </Form.Group>
    <Button variant="primary" type="submit">
      Подтвердить
    </Button>
    {client &&
    <Table striped bordered hover>
    <thead>
                  <tr>
                      <th>Имя</th>
                      <th>Телефон</th>
                  </tr>
              </thead>
              <tbody>
              <tr><td>{client.name}</td><td>{client.phone}</td></tr>
              </tbody>

    </Table>}
  </Form>

}

export default ClientById;