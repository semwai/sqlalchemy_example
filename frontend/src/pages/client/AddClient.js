import {Form, Button} from 'react-bootstrap'
import { useState, useEffect } from 'react'


function PostClient(name, phone, update) {
    console.log(update)
    fetch(`http://127.0.0.1:8000/client?name=${name}&phone=${phone}`, {
        method: 'POST',
    }).then(e => e.json())
      .then(e => {
          console.log(e)
          
          update()
        })
      .catch(e => console.log(e))
}

function AddClient(p) {
    const [name, setName] = useState("")
    const [phone, setPhone] = useState("")


    const submit = (e) => {
        e.preventDefault()
        PostClient(name, phone, p.method)
    }
    
    return <Form onSubmit={submit}>
    <Form.Group className="mb-3" controlId="formBasicName">
      <Form.Label>Имя клиента</Form.Label>
      <Form.Control type="text" value={name} onChange={e => setName(e.target.value)}/>
    </Form.Group>
  
    <Form.Group className="mb-3" controlId="formBasicPhone">
      <Form.Label>Телефон клиента</Form.Label>
      <Form.Control type="text" value={phone} onChange={e => {setPhone(e.target.value);console.log(e.target.value)}}/>
    </Form.Group>


    <Button variant="primary" type="submit">
      Подтвердить
    </Button>
  </Form>
}

export default AddClient