from fastapi import FastAPI, Depends, Path, HTTPException
from fastapi.middleware.cors import CORSMiddleware
from sqlalchemy import create_engine, select
from sqlalchemy.exc import IntegrityError
from sqlalchemy.orm import sessionmaker, Session
from typing import List, Optional
from datetime import datetime
import uvicorn
from configparser import ConfigParser
from pydantic import constr

from schemas import GetFood, GetClient, GetOrder, GetStates, GetFoodInOrder, PostFood
from models import Base, Food, Client, Order, FoodInOrder, OrderStates

config = ConfigParser()
config.read('../project.ini')

app = FastAPI()


app.add_middleware(
    CORSMiddleware,
    allow_origins=['http://localhost:3000'],
    allow_credentials=True,
    allow_methods=["*"],
    allow_headers=["*"],
)


engine = create_engine(config['project']['sql_connect'], echo=True)
# Base.metadata.create_all(engine)
SessionLocal = sessionmaker(autocommit=False, autoflush=False, bind=engine)


def get_db():
    db = SessionLocal()
    try:
        yield db
    finally:
        db.close()


@app.post('/food', tags=['Food'], response_model=GetFood)
def post_food(input: PostFood = Depends(), session: Session = Depends(get_db)):
    food = Food(name=input.name, price=input.price, weight=input.weight)
    try:

        session.add(food)
        session.commit()
    except IntegrityError:
        session.rollback()
        raise HTTPException(status_code=409, detail=f"Food {input.name} already exist")
    raise HTTPException(status_code=201, detail="Created")


@app.get('/food/{id}',  tags=['Food'])
def get_food(id: int, session: Session = Depends(get_db)):
    food = session.query(Food).filter_by(id=id).first()
    if food is None:
        raise HTTPException(status_code=404, detail="Food not found")
    return GetFood(id=food.id, weight=food.weight, name=food.name, price=food.price)


@app.get('/all_food/', tags=['Food'], response_model=List[GetFood])
def get_all_food(session: Session = Depends(get_db)):
    all_food = session.query(Food)
    return [GetFood(id=food.id, weight=food.weight, name=food.name, price=food.price) for food in all_food]


@app.post('/client', tags=['Client'])
def post_client(name: str, phone: constr(strip_whitespace=True, regex=r"^[\+]{0,1}[1-9][0-9-().]{9,15}$"),  session: Session = Depends(get_db)):
    try:
        client = Client(name=name, phone=phone)
        session.add(client)
        session.commit()
        raise HTTPException(status_code=201, detail="Created")
    except IntegrityError as e:
        session.rollback()
        raise HTTPException(status_code=409, detail="User already exist")


@app.get('/client/{id}',  tags=['Client'])
def get_client(id: int, session: Session = Depends(get_db)):
    client = session.query(Client).filter_by(id=id).first()
    if client is None:
        raise HTTPException(status_code=404, detail="Client not found")
    return GetClient(id=client.id, name=client.name, phone=client.phone)


@app.get('/clients', response_model=List[GetClient], tags=['Client'])
def get_clients(session: Session = Depends(get_db)):
    clients = session.query(Client)
    return [GetClient(id=client.id, name=client.name, phone=client.phone) for client in clients]


@app.post('/order', tags=['Order'])
def post_order(user_id: int = 0, date: Optional[datetime] = None, session: Session = Depends(get_db)):
    if session.query(Client).filter_by(id=user_id).first() is None:
        raise HTTPException(status_code=404, detail="Client not found")
    new_date = datetime.now() if date is None else date
    order = Order(user_id=user_id, date=new_date)
    session.add(order)
    session.commit()
    raise HTTPException(status_code=201, detail=str(order.id))
    

@app.get('/order/{id}',  tags=['Order'])
def get_order(id: int, session: Session = Depends(get_db)):
    order = session.query(Order).filter_by(id=id).first()
    if order is None:
        raise HTTPException(status_code=404, detail="Order not found")
    return GetOrder(id=order.id, date=order.date, state=order.state.name, client=order.client.__dict__)


@app.get('/orders/', response_model=List[GetOrder], tags=['Order'])
def get_orders(filter: OrderStates = None, session: Session = Depends(get_db)):
    if filter:
        orders = session.query(Order).filter_by(state=filter)
        if orders is None:
            raise HTTPException(status_code=404, detail=f"Orders not found with this filter - {filter}")
        return [GetOrder(id=order.id, date=order.date, state=order.state.name, client=order.client.__dict__) for order in orders]
    else:
        orders = session.query(Order)
        if orders is None:
            raise HTTPException(status_code=404, detail="Orders not found")
        return [GetOrder(id=order.id, date=order.date, state=order.state.name, client=order.client.__dict__) for order in orders]


@app.get('/foodInOrder', response_model=List[GetFoodInOrder], tags=['Order'])
def get_food_in_order(order_id: int, session: Session = Depends(get_db)):
    all_food = session.query(FoodInOrder).filter_by(order_id=order_id)
    if all_food.first() is None:
        raise HTTPException(status_code=404, detail=f"Food not found in this order - {order_id}")
    return [GetFoodInOrder(order_id=food.order_id, food_id=food.food_id, count=food.count, price=food.price) for food in all_food]


@app.post('/addToOrder', tags=['Order'])
def add_food_to_order(order_id: int, food_id: int, count: int = 1, session: Session = Depends(get_db)):
    food = session.query(Food).filter_by(id=food_id).first()
    if food is None:
        raise HTTPException(status_code=404, detail="Food not found")
    order = session.query(Order).filter_by(id=order_id).first()
    if order is None:
        raise HTTPException(status_code=404, detail="Order not found")
    elif order.state.name != OrderStates._member_names_[0]:
        raise HTTPException(status_code=422, detail="Order.state doesn't equals "+OrderStates._member_names_[0])
    foodInOrder = FoodInOrder(food_id=food_id, order_id=order_id, count=count, price=count*food.price)
    session.add(foodInOrder)
    session.commit()
    return HTTPException(status_code=201, detail=f"Food successfully added to order {order.id}")


@app.post('/changeOrderStatus', tags=['Order'])
def change_order_status(order_id: int, changed_status: OrderStates, session: Session = Depends(get_db)):
    order = session.query(Order).filter_by(id=order_id).first()
    if order:
        session.query(Order).filter(Order.id == order_id).update({"state": changed_status})
        session.commit()
        raise HTTPException(status_code=201, detail=f"Status has successfully changed to {changed_status.name}")


@app.get('/states/', tags=['Order'])
def get_states():
    return OrderStates._member_names_




if __name__ == '__main__':
    uvicorn.run("main:app", host='0.0.0.0', port=8000, reload=True)
