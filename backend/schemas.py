from pydantic import BaseModel, validator
from typing import Optional
from datetime import datetime


class PostFood(BaseModel):
    name: str
    price: float
    weight: int = 0

    @validator('price')
    def price_greater_than_zero(cls, v):
        if v < 0:
            raise ValueError('price greater than zero')
        return v


class GetFood(BaseModel):
    id: int
    weight: Optional[int]
    name: str 
    price: float
    

class GetClient(BaseModel):
    id: int
    name: str 
    phone: str
    

class GetOrder(BaseModel):
    id: int
    date: datetime
    state: str
    client: GetClient


class GetFoodInOrder(BaseModel):
    order_id: int
    food_id: int
    count: int
    price: float


class GetStates(BaseModel):
    state: str