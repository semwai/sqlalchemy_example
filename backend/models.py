from sqlalchemy import Column, Integer, String, ForeignKey, Numeric, DateTime, Enum
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.orm import relationship
import enum


Base = declarative_base()


class Food(Base):
    __tablename__ = "Food"
    id = Column(Integer, primary_key=True, autoincrement=True)
    name = Column(String(50), nullable=False, unique=True)
    price = Column(Numeric(10, 2), nullable=False)
    weight = Column(Integer)


class Client(Base):
    __tablename__ = "Client"
    id = Column(Integer, primary_key=True, autoincrement=True)
    name = Column(String(50), nullable=False, unique=True)
    phone = Column(String(50), nullable=False, unique=True)


class OrderStates(enum.Enum):
    Created = "Created"
    In_progress = "In_progress"
    Finished = "Finished"
    Closed = "Closed"


class Order(Base):
    __tablename__ = "Order"
    id = Column(Integer, primary_key=True, autoincrement=True)
    user_id = Column(Integer, ForeignKey('Client.id', ondelete='RESTRICT'))
    date = Column(DateTime, nullable=False)
    #state = Column(String(50), nullable=False, server_default='Created')
    state = Column(Enum(OrderStates), nullable=False, default=OrderStates.Created.name)
    client = relationship("Client")
    foods = relationship("FoodInOrder")


class FoodInOrder(Base):
    __tablename__ = "FoodInOrder"
    order_id = Column(Integer, ForeignKey('Order.id', ondelete='RESTRICT'), primary_key=True)
    food_id = Column(Integer, ForeignKey('Food.id', ondelete='RESTRICT'), primary_key=True)
    count = Column(Integer, nullable=False)
    price = Column(Numeric(10, 2), nullable=False)
